##############################################################################
#
# Copyright (C) 2014 <devel@dave.eu>)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE or
# NON INFRINGEMENT.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
##############################################################################

"""
ToloMEO Edge custom service example
This service retrieves data from the local instance of Glances and upload them to ToloMEO cloud.
For more details, please see https://tolomeo.io/2024/03/30/connecting-grafana-to-tolomeo/.
"""

import argparse
import asyncio
import logging
import random
import signal
import sys
from time import time

import nats
import requests
from nats.aio import client, msg

SERVICE_NAME = "glances_stats"

SERVICE_VERSION = "1.0.0"

# Default values, can be changed with command line arguments
GLANCES_API_URL = "http://localhost:61208/api/3"
SLEEP_TIME = 5  # Seconds between messages

IMPLEMENTED_PLUGINS = ["load", "cpu", "processcount", "mem", "memswap"]

LOGGER = logging.getLogger(__name__)


async def start_heartbeat(nc: client.Client) -> None:
    while True:
        LOGGER.info("Publishing heartbeat...")
        # Any custom logic that checks that everything is ok should be added here.
        await nc.publish(f"heartbeat.{SERVICE_NAME}.service")
        await asyncio.sleep(10_000 / 1000.0)


async def start_update_loop(nc: client.Client) -> None:
    while True:
        # Build a SenML message
        assert check_glances_api(), "Glances API unavailable"

        infos = []
        for plugin in IMPLEMENTED_PLUGINS:
            infos.extend(get_plugin(plugin, f"{SERVICE_NAME}:{plugin}:"))

        msg = senml_string_constructor(infos)
        LOGGER.info(f"Sending message: {msg}")

        await nc.publish("events.data", msg.encode("utf-8"))
        await asyncio.sleep(SLEEP_TIME)


async def main() -> None:
    async def handle_exit():
        await sub.unsubscribe()
        update_loop_task.cancel()
        heartbeat_task.cancel()
        sys.exit(0)

    for signame in ["SIGINT", "SIGQUIT", "SIGTERM"]:
        asyncio.get_event_loop().add_signal_handler(
            getattr(signal, signame),
            lambda: asyncio.create_task(handle_exit()),
        )

    logging.basicConfig(level=logging.INFO)
    nc = await nats.connect()

    async def handler(msg: msg.Msg):
        subject = msg.subject
        data = msg.data.decode()
        LOGGER.info("Received command: [%s] %s", subject, data)

    sub = await nc.subscribe(f"commands.{SERVICE_NAME}.>", cb=handler)

    if sys.version_info >= (3, 11):
        async with asyncio.TaskGroup() as tg:
            update_loop_task = tg.create_task(start_update_loop(nc))
            heartbeat_task = tg.create_task(start_heartbeat(nc))
    else:
        update_loop_task = asyncio.create_task(start_update_loop(nc))
        heartbeat_task = asyncio.create_task(start_heartbeat(nc))
        await asyncio.gather(update_loop_task, heartbeat_task)

def check_glances_api() -> bool:
    try:
        requests.get(GLANCES_API_URL + "/status")
        return True
    except requests.exceptions.ConnectionError:
        return False


def senml_string_constructor(info_list: list) -> str:
    assert len(info_list) > 0, "At least one field is required"

    detection_time = time()
    baseline_string = "["

    fields_data = []
    for record in info_list:
        field_name, field_value, value_type, unit = record
        if value_type == "number":
            fields_data.append(
                f'{{"n": "{field_name}", "v": {field_value}, "u": "{unit}", "t": {detection_time}}}'
            )
        elif value_type == "string":
            fields_data.append(
                f'{{"n": "{field_name}", "vs": "{field_value}", "u": "{unit}", "t": {detection_time}}}'
            )
        elif value_type == "boolean":
            fields_data.append(
                f'{{"n": "{field_name}", "vb": {field_value}, "u": "{unit}", "t": {detection_time}}}'
            )
        elif value_type == "base64":
            fields_data.append(
                f'{{"n": "{field_name}", "vd": "{field_value}", "u": "{unit}"}}, "t": {detection_time}'
            )
        else:
            raise ValueError(f"Unknown value type: {value_type}")

    if fields_data:
        baseline_string += ", ".join(fields_data)

    baseline_string += "]"

    return baseline_string


def get_plugin(plugin: str, prefix="") -> list:
    assert plugin in IMPLEMENTED_PLUGINS, f"Plugin {plugin} not implemented"

    response = requests.get(f"{GLANCES_API_URL}/{plugin}")
    data = response.json()

    return parse_plugin_data(plugin, data, prefix)


def parse_plugin_data(plugin: str, data: dict, prefix: str) -> list:
    assert plugin in IMPLEMENTED_PLUGINS, f"Plugin {plugin} not implemented"
    assert data, "Data is empty"

    parsed_info = []

    if plugin == "load":
        parsed_info.append((f"{prefix}cpucore", data["cpucore"], "number", "number"))
        parsed_info.append((f"{prefix}min1", data["min1"], "number", "float"))
        parsed_info.append((f"{prefix}min5", data["min5"], "number", "float"))
        parsed_info.append((f"{prefix}min15", data["min15"], "number", "float"))
    elif plugin == "cpu":
        parsed_info.append((f"{prefix}cpucore", data["cpucore"], "number", "number"))
        parsed_info.append(
            (f"{prefix}ctx_switches", data["ctx_switches"], "number", "number")
        )
        parsed_info.append((f"{prefix}guest", data["guest"], "number", "seconds"))
        parsed_info.append(
            (f"{prefix}guest_nice", data["guest_nice"], "number", "seconds")
        )
        parsed_info.append((f"{prefix}idle", data["idle"], "number", "percent"))
        parsed_info.append(
            (f"{prefix}interrupts", data["interrupts"], "number", "number")
        )
        parsed_info.append((f"{prefix}iowait", data["iowait"], "number", "percent"))
        parsed_info.append((f"{prefix}irq", data["irq"], "number", "percent"))
        parsed_info.append((f"{prefix}nice", data["nice"], "number", "percent"))
        parsed_info.append(
            (f"{prefix}soft_interrupts", data["soft_interrupts"], "number", "number")
        )
        parsed_info.append((f"{prefix}softirq", data["softirq"], "number", "seconds"))
        parsed_info.append((f"{prefix}steal", data["steal"], "number", "percent"))
        parsed_info.append((f"{prefix}syscalls", data["syscalls"], "number", "number"))
        parsed_info.append((f"{prefix}system", data["system"], "number", "percent"))
        parsed_info.append(
            (
                f"{prefix}time_since_update",
                data["time_since_update"],
                "number",
                "seconds",
            )
        )
        parsed_info.append((f"{prefix}total", data["total"], "number", "percent"))
        parsed_info.append((f"{prefix}user", data["user"], "number", "percent"))
    elif plugin == "processcount":
        parsed_info.append((f"{prefix}pid_max", data["pid_max"], "number", "number"))
        parsed_info.append((f"{prefix}running", data["running"], "number", "number"))
        parsed_info.append((f"{prefix}sleeping", data["sleeping"], "number", "number"))
        parsed_info.append((f"{prefix}thread", data["thread"], "number", "number"))
        parsed_info.append((f"{prefix}total", data["total"], "number", "number"))
    elif plugin == "mem":
        parsed_info.append((f"{prefix}active", data["active"], "number", "bytes"))
        parsed_info.append((f"{prefix}available", data["available"], "number", "bytes"))
        parsed_info.append((f"{prefix}buffers", data["buffers"], "number", "bytes"))
        parsed_info.append((f"{prefix}cached", data["cached"], "number", "bytes"))
        parsed_info.append((f"{prefix}free", data["free"], "number", "bytes"))
        parsed_info.append((f"{prefix}inactive", data["inactive"], "number", "bytes"))
        parsed_info.append((f"{prefix}percent", data["percent"], "number", "percent"))
        parsed_info.append((f"{prefix}shared", data["shared"], "number", "bytes"))
        parsed_info.append((f"{prefix}total", data["total"], "number", "bytes"))
        parsed_info.append((f"{prefix}used", data["used"], "number", "bytes"))
    elif plugin == "memswap":
        parsed_info.append((f"{prefix}free", data["free"], "number", "bytes"))
        parsed_info.append((f"{prefix}percent", data["percent"], "number", "percent"))
        parsed_info.append((f"{prefix}sin", data["sin"], "number", "bytes"))
        parsed_info.append((f"{prefix}sout", data["sout"], "number", "bytes"))
        parsed_info.append(
            (
                f"{prefix}time_since_update",
                data["time_since_update"],
                "number",
                "seconds",
            )
        )
        parsed_info.append((f"{prefix}total", data["total"], "number", "bytes"))
        parsed_info.append((f"{prefix}used", data["used"], "number", "bytes"))
    else:
        raise ValueError(f"Plugin {plugin} not implemented")

    return parsed_info


def get_fake_plugin(plugin: str, prefix=""):
    assert plugin in IMPLEMENTED_PLUGINS, f"Plugin {plugin} not implemented"

    return parse_fake_plugin_data(plugin, prefix)


def parse_fake_plugin_data(plugin: str, prefix: str):
    assert plugin in IMPLEMENTED_PLUGINS, f"Plugin {plugin} not implemented"

    parsed_info = []

    if plugin == "load":
        parsed_info.append((f"{prefix}cpucore", 4, "number", "number"))
        parsed_info.append((f"{prefix}min1", random.uniform(0, 9), "number", "float"))
        parsed_info.append((f"{prefix}min5", random.uniform(0, 15), "number", "float"))
        parsed_info.append((f"{prefix}min15", random.uniform(0, 40), "number", "float"))
    elif plugin == "cpu":
        parsed_info.append((f"{prefix}cpucore", 4, "number", "number"))
        parsed_info.append(
            (f"{prefix}ctx_switches", random.randint(0, 10e6), "number", "number")
        )
        parsed_info.append(
            (f"{prefix}guest", random.uniform(0, 10e4), "number", "seconds")
        )
        parsed_info.append(
            (f"{prefix}guest_nice", random.uniform(0, 10e4), "number", "seconds")
        )
        parsed_info.append(
            (f"{prefix}idle", random.uniform(0, 100), "number", "percent")
        )
        parsed_info.append(
            (f"{prefix}interrupts", random.randint(0, 100), "number", "number")
        )
        parsed_info.append(
            (f"{prefix}iowait", random.uniform(0, 100), "number", "percent")
        )
        parsed_info.append(
            (f"{prefix}irq", random.uniform(0, 100), "number", "percent")
        )
        parsed_info.append(
            (f"{prefix}nice", random.uniform(0, 100), "number", "percent")
        )
        parsed_info.append(
            (f"{prefix}soft_interrupts", random.randint(10, 10e4), "number", "number")
        )
        parsed_info.append(
            (f"{prefix}softirq", random.uniform(10, 10e4), "number", "seconds")
        )
        parsed_info.append(
            (f"{prefix}steal", random.uniform(0, 100), "number", "percent")
        )
        parsed_info.append(
            (f"{prefix}syscalls", random.randint(0, 500), "number", "number")
        )
        parsed_info.append(
            (f"{prefix}system", random.uniform(0, 100), "number", "percent")
        )
        parsed_info.append(
            (f"{prefix}time_since_update", random.uniform(0, 200), "number", "seconds")
        )
        parsed_info.append(
            (f"{prefix}total", random.uniform(80, 100), "number", "percent")
        )
        parsed_info.append(
            (f"{prefix}user", random.uniform(0, 100), "number", "percent")
        )
    elif plugin == "processcount":
        parsed_info.append(
            (f"{prefix}pid_max", random.randint(0, 100), "number", "number")
        )
        parsed_info.append(
            (f"{prefix}running", random.randint(0, 100), "number", "number")
        )
        parsed_info.append(
            (f"{prefix}sleeping", random.randint(0, 100), "number", "number")
        )
        parsed_info.append(
            (f"{prefix}thread", random.randint(0, 100), "number", "number")
        )
        parsed_info.append(
            (f"{prefix}total", random.randint(0, 100), "number", "number")
        )
    elif plugin == "mem":
        parsed_info.append(
            (f"{prefix}active", random.randint(0, 100), "number", "bytes")
        )
        parsed_info.append(
            (f"{prefix}available", random.randint(0, 100), "number", "bytes")
        )
        parsed_info.append(
            (f"{prefix}buffers", random.randint(0, 100), "number", "bytes")
        )
        parsed_info.append(
            (f"{prefix}cached", random.randint(0, 100), "number", "bytes")
        )
        parsed_info.append((f"{prefix}free", random.randint(0, 100), "number", "bytes"))
        parsed_info.append(
            (f"{prefix}inactive", random.randint(0, 100), "number", "bytes")
        )
        parsed_info.append(
            (f"{prefix}percent", random.uniform(0, 100), "number", "percent")
        )
        parsed_info.append(
            (f"{prefix}shared", random.randint(0, 100), "number", "bytes")
        )
        parsed_info.append(
            (f"{prefix}total", random.randint(0, 100), "number", "bytes")
        )
        parsed_info.append((f"{prefix}used", random.randint(0, 100), "number", "bytes"))
    elif plugin == "memswap":
        parsed_info.append((f"{prefix}free", random.randint(0, 100), "number", "bytes"))
        parsed_info.append(
            (f"{prefix}percent", random.uniform(0, 100), "number", "percent")
        )
        parsed_info.append((f"{prefix}sin", random.randint(0, 100), "number", "bytes"))
        parsed_info.append((f"{prefix}sout", random.randint(0, 100), "number", "bytes"))
        parsed_info.append(
            (f"{prefix}time_since_update", random.uniform(0, 100), "number", "seconds")
        )
        parsed_info.append(
            (f"{prefix}total", random.randint(0, 100), "number", "bytes")
        )
        parsed_info.append((f"{prefix}used", random.randint(0, 100), "number", "bytes"))
    else:
        raise ValueError(f"Plugin {plugin} not implemented")

    return parsed_info


def get_info(url: str) -> dict:
    response = requests.get(url)

    data = response.json()
    return data


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Send glances data to the cloud.")
    parser.add_argument(
        "-t",
        "--update_time",
        help="Seconds between messages",
        type=int,
        default=SLEEP_TIME,
    )
    parser.add_argument(
        "-g", "--glances", help="Glances API URL", type=str, default=GLANCES_API_URL
    )

    args = parser.parse_args()

    GLANCES_API_URL = args.glances
    SLEEP_TIME = args.update_time

    asyncio.run(main())