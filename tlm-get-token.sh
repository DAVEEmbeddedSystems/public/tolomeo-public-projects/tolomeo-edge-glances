#!/bin/bash

INSTANCE=$1
USER=$2
PASS=$3


actionUrl=$(
  curl -s -X GET -H "Accept: application/json" \
    "https://accounts.$INSTANCE/auth/self-service/login/api" |
    jq -r '.ui.action'
)


if [ -z "$actionUrl" ]; then
  echo "Unable to get login action URL"
  exit 1
fi

#read -rp 'Username: ' USER
#read -rsp 'Password: ' PASS

session=$(
  curl -s -X POST -H "Accept: application/json" -H "Content-Type: application/json" \
    -d "{\"identifier\": \"$USER\", \"password\": \"$PASS\", \"method\": \"password\"}" \
    "$actionUrl"
)

if [ -z "$session" ]; then
  echo "Unable to login"
  exit 1
fi

name=$(echo "$session" | jq -r '.session.identity.traits.name | .first + " " + .last')
token=$(echo "$session" | jq -r '.session_token')

echo "\nWelcome $name!"
echo
echo "Your session token is: $token"